package main

import (
	"bytes"
	"encoding/json"
	"flag"
	"fmt"
	"github.com/sirupsen/logrus"
	"io"
	"net/http"
	"os"
)

func main() {
	logObj := logrus.New()
	logObj.Formatter = new(logrus.JSONFormatter)
	logObj.Level = logrus.TraceLevel
	logObj.Out = os.Stdout

	url := flag.String("url", "", "url for hook")
	secretToken := flag.String("secret-token", "", "secret token for hook")
	apiKey := flag.String("api-key", "", "api-key for request")
	flag.Parse()

	if *url == "" {
		logrus.Fatal("Url was not provided")
	}

	if *secretToken == "" {
		logrus.Fatal("Secret token was not provided")
	}

	if *apiKey == "" {
		logrus.Fatal("Api key was not provided")
	}

	requestUrl := fmt.Sprintf("https://api.telegram.org/bot%s/%s", *apiKey, "setWebhook")
	type requestBody struct {
		Url         string `json:"url"`
		SecretToken string `json:"secret_token"`
	}

	requestData := &requestBody{
		Url:         *url,
		SecretToken: *secretToken,
	}

	var buf bytes.Buffer
	err := json.NewEncoder(&buf).Encode(requestData)
	if err != nil {
		logrus.Fatal(err)
	}

	client := &http.Client{}
	request, err := http.NewRequest("POST", requestUrl, &buf)
	if err != nil {
		logrus.Fatal(err)
	}

	request.Header.Set("Content-Type", "application/json")
	request.Header.Set("Charset", "UTF8")
	response, _ := client.Do(request)
	defer response.Body.Close()
	if response.StatusCode != http.StatusOK {
		var errData string
		bodyData, _ := io.ReadAll(response.Body)
		errData = string(bodyData)

		errMsg := fmt.Sprintf("Request was not success. Error code: %d, error message: %s", response.StatusCode, errData)
		logrus.Fatal(errMsg)
	}

	logrus.Println("Webhook set successfully")
}
