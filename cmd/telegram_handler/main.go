package main

import (
	"PetTgBot/api/telegram_handler"
	"PetTgBot/internal/services"
	"PetTgBot/internal/storage/postgres"
	"PetTgBot/internal/telegram"
	"context"
	"encoding/json"
	"github.com/gorilla/mux"
	"github.com/sirupsen/logrus"
	"log"
	"net/http"
	"os"
	"os/signal"
	"syscall"
)

var telegramHandler telegram.HandlerInterface

func main() {
	logObj := logrus.New()
	logObj.Formatter = new(logrus.JSONFormatter)
	logObj.Level = logrus.TraceLevel
	logObj.Out = os.Stdout

	telegramApiKey := os.Getenv("TELEGRAM_APIKEY")
	if telegramApiKey == "" {
		logrus.Fatal("Telegram api key was not provide")
	}

	pocketConsumerKey := os.Getenv("POCKET_CONSUMER_KEY")
	if pocketConsumerKey == "" {
		logrus.Fatal("Pocket consumer key was not provide")
	}

	pocketCallbackUrl := os.Getenv("POCKET_CALLBACK_URL")
	if pocketCallbackUrl == "" {
		logrus.Fatal("Pocket callback url was not provide")
	}

	dbCnf := &postgres.Config{
		Username:      os.Getenv("POSTGRES_USERNAME"),
		Password:      os.Getenv("POSTGRES_PASSWORD"),
		Host:          os.Getenv("POSTGRES_HOST"),
		Port:          os.Getenv("POSTGRES_PORT"),
		DbName:        os.Getenv("POSTGRES_DBNAME"),
		MaxConnection: os.Getenv("POSTGRES_MAX_CONNECTIONS"),
	}

	db, err := postgres.NewDb(dbCnf)
	if err != nil {
		log.Fatal(err)
	}

	defer db.Close()
	service := services.NewTelegramService(db)
	sender := telegram.NewSender(telegramApiKey)
	pocketCnf := telegram.NewPocketCnf(pocketConsumerKey, pocketCallbackUrl)
	telegramHandler = telegram.NewHandler(service, sender, pocketCnf)
	serverCnf := &telegram_handler.Config{
		Port: 8082,
	}

	router := mux.NewRouter()
	router.HandleFunc("/", handler).Methods("POST")

	server := telegram_handler.New(serverCnf, router)
	go server.Run()

	stopCh, closeCh := createChannel()
	defer closeCh()
	logrus.Println("stop signal received:", <-stopCh)

	server.Shutdown(context.Background())
}

func handler(w http.ResponseWriter, r *http.Request) {
	message := &telegram.Message{}
	err := json.NewDecoder(r.Body).Decode(&message)
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		logrus.Errorf("Error whiling parse message: %v", err)
		return
	}

	if message == nil {
		w.WriteHeader(http.StatusNoContent)
		return
	}

	err = telegramHandler.ProcessCommand(message)
	if err != nil {
		logrus.Errorf("Error whiling handle: %v", err)
	}

	w.WriteHeader(http.StatusOK)
	return
}

func createChannel() (chan os.Signal, func()) {
	stopCh := make(chan os.Signal, 1)
	signal.Notify(stopCh, syscall.SIGTERM, syscall.SIGINT)

	return stopCh, func() {
		close(stopCh)
	}
}
