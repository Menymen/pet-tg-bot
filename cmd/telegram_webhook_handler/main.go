package main

import (
	"PetTgBot/api/telegram_webhook"
	"PetTgBot/internal/telegram"
	"bytes"
	"context"
	"encoding/json"
	"fmt"
	"github.com/gorilla/mux"
	"github.com/sirupsen/logrus"
	"io"
	"net/http"
	"os"
	"os/signal"
	"syscall"
	"time"
)

var handlerHost string

func main() {
	logObj := logrus.New()
	logObj.Formatter = new(logrus.JSONFormatter)
	logObj.Level = logrus.TraceLevel
	logObj.Out = os.Stdout

	secretToken, exist := os.LookupEnv("TELEGRAM_SECRET_TOKEN")
	if !exist {
		logrus.Fatal("Secret token wasn't set")
	}

	handlerHostEnv, exist := os.LookupEnv("TELEGRAM_HANDLER_HOST")
	if !exist {
		logrus.Fatal("Handler host wasn't set")
	}

	handlerHost = handlerHostEnv
	router := mux.NewRouter()
	router.HandleFunc("/", handler).Methods("POST")

	handler := checkSecretToken(router, secretToken)
	cnf := &telegram_webhook.Config{
		Port: 8081,
	}

	server := telegram_webhook.New(cnf, handler)
	go server.Run()

	stopCh, closeCh := createChannel()
	defer closeCh()
	logrus.Println("stop signal received:", <-stopCh)

	server.Shutdown(context.Background())
}

func handler(w http.ResponseWriter, r *http.Request) {
	messageUpdate := &telegram.MessageUpdate{}
	err := json.NewDecoder(r.Body).Decode(&messageUpdate)
	if err != nil {
		logrus.Errorf("Error whiling parse webhook message: %v", err)
		return
	}

	var buf bytes.Buffer
	err = json.NewEncoder(&buf).Encode(messageUpdate.Message)
	if err != nil {
		logrus.Error(err)
		return
	}

	client := &http.Client{
		Timeout: 5 * time.Second,
	}

	request, err := http.NewRequest("POST", handlerHost, &buf)
	if err != nil {
		logrus.Error(err)
		return
	}

	request.Header.Set("Content-Type", "application/json")
	request.Header.Set("Charset", "UTF8")

	response, err := client.Do(request)
	if response != nil {
		defer response.Body.Close()
		if response.StatusCode != http.StatusOK {
			var errData string
			bodyData, _ := io.ReadAll(response.Body)
			errData = string(bodyData)

			errMsg := fmt.Sprintf("Request was not success. Error code: %d, error message: %s", response.StatusCode, errData)
			logrus.Errorln(errMsg)
			return
		}
	} else {
		logrus.Errorln("Empty response")
	}

	w.WriteHeader(http.StatusOK)
}

func checkSecretToken(next http.Handler, secretToken string) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, req *http.Request) {
		requestSecretToken := req.Header.Get("X-Telegram-Bot-Api-Secret-Token")
		if requestSecretToken != secretToken {
			http.Error(w, http.StatusText(http.StatusNotFound), http.StatusNotFound)
		}

		next.ServeHTTP(w, req)
	})
}

func createChannel() (chan os.Signal, func()) {
	stopCh := make(chan os.Signal, 1)
	signal.Notify(stopCh, syscall.SIGTERM, syscall.SIGINT)

	return stopCh, func() {
		close(stopCh)
	}
}
