package main

import (
	"PetTgBot/api/pocket"
	pocketSdk "PetTgBot/pkg/pocket"
	"context"
	"fmt"
	"github.com/gorilla/mux"
	"github.com/sirupsen/logrus"
	"net/http"
	"os"
	"os/signal"
	"syscall"
)

var consumerKey string

func main() {
	logObj := logrus.New()
	logObj.Formatter = new(logrus.JSONFormatter)
	logObj.Level = logrus.TraceLevel
	logObj.Out = os.Stdout

	var exist bool
	consumerKey, exist = os.LookupEnv("POCKET_CONSUMER_KEY")
	if !exist {
		logrus.Fatalln("consumer key was not set")
	}

	router := mux.NewRouter()
	router.HandleFunc("/", handler).Methods("GET")
	cnf := &pocket.Config{
		Port: 8080,
	}

	server := pocket.New(cnf, router)
	go server.Run()

	stopCh, closeCh := createChannel()
	defer closeCh()
	logrus.Println("stop signal received:", <-stopCh)

	server.Shutdown(context.Background())
}

func handler(w http.ResponseWriter, r *http.Request) {
	//todo Определения пользователя по state
	query := r.URL.Query()
	stateArray, exists := query["state"]
	if !exists || len(stateArray) == 0 {
		logrus.Error("Empty state")
		w.WriteHeader(http.StatusOK)
		return
	}

	state := pocketSdk.ParseState(stateArray[0])
	w.WriteHeader(http.StatusNoContent)
	return
	accessToken, err := pocketSdk.RequestAccessToken(consumerKey, state["tokenCode"])
	if err != nil {
		logrus.Println(err)
		w.WriteHeader(http.StatusInternalServerError)
		return
	}

	fmt.Println(accessToken)
	w.WriteHeader(http.StatusOK)
	return
	//todo save accessToken
}

func createChannel() (chan os.Signal, func()) {
	stopCh := make(chan os.Signal, 1)
	signal.Notify(stopCh, syscall.SIGTERM, syscall.SIGINT)

	return stopCh, func() {
		close(stopCh)
	}
}
