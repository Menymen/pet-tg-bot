BEGIN;

DROP TABLE IF EXISTS "telegram"."message";
DROP TABLE IF EXISTS "telegram"."chat";
DROP TABLE IF EXISTS "telegram"."from";
DROP TABLE IF EXISTS "user";
DROP SCHEMA IF EXISTS "telgram";

COMMIT;