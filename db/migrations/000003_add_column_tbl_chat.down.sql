BEGIN;

ALTER TABLE telegram.chat DROP COLUMN access_token;
ALTER TABLE telegram.chat DROP COLUMN token_code;

COMMIT;