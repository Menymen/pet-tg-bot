BEGIN;

ALTER TABLE telegram.chat ADD COLUMN access_token varchar(255);
ALTER TABLE telegram.chat ADD COLUMN token_code varchar(255);

COMMIT;