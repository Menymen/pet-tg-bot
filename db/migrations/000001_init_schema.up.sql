CREATE SCHEMA IF NOT EXISTS "public";

BEGIN;

CREATE SCHEMA IF NOT EXISTS "telegram";

CREATE TABLE "public"."user" (
    "id" serial primary key,
    "auth_key" varchar(255) not null,
    "email" varchar(255) null,
    "password" varchar(255) null,
    "created_at" timestamp with time zone NOT NULL DEFAULT CURRENT_TIMESTAMP,
    "updated_at" timestamp with time zone NOT NULL DEFAULT CURRENT_TIMESTAMP
);

CREATE TABLE "telegram"."from" (
    "id" bigint primary key,
    "is_bot" bool not null,
    "first_name" varchar(255) not null,
    "last_name" varchar(255) not null,
    "username" varchar(255) not null,
    "language_code" varchar(255) not null,
    "is_premium" bool not null,
    "created_at" timestamp with time zone NOT NULL DEFAULT CURRENT_TIMESTAMP,
    "updated_at" timestamp with time zone NOT NULL DEFAULT CURRENT_TIMESTAMP
);

CREATE TABLE "telegram"."chat" (
     "id" bigint primary key,
     "first_name" varchar(255) not null,
     "last_name" varchar(255) not null,
     "type" varchar(255) not null,
     "created_at" timestamp with time zone NOT NULL DEFAULT CURRENT_TIMESTAMP,
     "updated_at" timestamp with time zone NOT NULL DEFAULT CURRENT_TIMESTAMP
);

CREATE TABLE "telegram"."message" (
    "id" bigint primary key,
    "from_id" bigint not null,
    "chat_id" bigint not null,
    "date" timestamp with time zone NOT NULL,
    "text" text not null,
    "created_at" timestamp with time zone NOT NULL DEFAULT CURRENT_TIMESTAMP,
    "updated_at" timestamp with time zone NOT NULL DEFAULT CURRENT_TIMESTAMP,

    constraint fk__TelegramMessage_fromId__TelegramFrom_id
        foreign key (from_id) references "telegram"."from" (id)
            ON DELETE CASCADE ON UPDATE CASCADE,
    constraint fk__TelegramMessage_chatId__TelegramChat_id
        foreign key (chat_id) references "telegram"."chat" (id)
            ON DELETE CASCADE ON UPDATE CASCADE
);

COMMIT;