BEGIN;

ALTER TABLE telegram.chat ADD COLUMN is_enabled bool;
UPDATE telegram.chat SET is_enabled = true;
ALTER TABLE telegram.chat ALTER COLUMN is_enabled SET NOT NULL;

COMMIT;