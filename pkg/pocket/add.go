package pocket

import (
	"errors"
	"strings"
)

type AddLinkRequest struct {
	Url         string   `json:"url"`
	Title       string   `json:"title,omitempty"`
	TagsSlice   []string `json:"-"`
	ConsumerKey string   `json:"consumer_key"`
	AccessToken string   `json:"access_token"`
	Tags        string   `json:"tags,omitempty"`
}

func (r *AddLinkRequest) Validate() error {
	if r.ConsumerKey == "" {
		return errors.New("consumer key is required")
	}

	if r.AccessToken == "" {
		return errors.New("access key is required")
	}

	if r.Url == "" {
		return errors.New("url is required")
	}

	return nil
}

func (r *AddLinkRequest) Prepare() {
	if len(r.TagsSlice) > 0 {
		r.Tags = strings.Join(r.TagsSlice, ",")
	}
}
