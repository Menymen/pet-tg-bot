package pocket

import (
	"errors"
	"sort"
)

type (
	AuthorizeDataRequest struct {
		ConsumerKey    string `json:"consumer_key"`
		RedirectUrl    string `json:"redirect_uri"`
		State          map[string]string
		ConvertedState string `json:"state"`
	}

	AuthorizeDataResponse struct {
		AccessToken string `json:"access_token"`
		Username    string `json:"username"`
	}

	ConvertTokenToAccessToken struct {
		ConsumerKey string `json:"consumer_key"`
		Code        string `json:"code"`
	}
)

func (authData *AuthorizeDataRequest) Validate() error {
	if authData.ConsumerKey == "" {
		return errors.New("consumer key is required")
	}

	if authData.RedirectUrl == "" {
		return errors.New("redirect uri is required")
	}

	return nil
}

func (authData *AuthorizeDataRequest) EncodeState() {
	var stateStr string
	keys := make([]string, 0, len(authData.State))
	for key := range authData.State {
		keys = append(keys, key)
	}

	sort.Strings(keys)
	for _, key := range keys {
		if key == "" || authData.State[key] == "" {
			continue
		}

		if stateStr != "" {
			stateStr += ";"
		}

		stateStr += key + ":" + authData.State[key]
	}

	authData.ConvertedState = stateStr
}
