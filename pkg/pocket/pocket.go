package pocket

import (
	"bytes"
	"encoding/json"
	"errors"
	"fmt"
	"io"
	"net/http"
	"strings"
)

const (
	apiUrl = "https://getpocket.com/v3/"

	requestTokenAction = "oauth/request"
	accessTokenAction  = "oauth/authorize"
	addAction          = "add"
	retrieveAction     = "get"
)

func NewAuthData(consumerKey string, redirectUrl string, state map[string]string) *AuthorizeDataRequest {
	authData := &AuthorizeDataRequest{
		ConsumerKey: consumerKey,
		State:       state,
		RedirectUrl: redirectUrl,
	}

	authData.EncodeState()
	return authData
}

func RequestTokenCode(authData *AuthorizeDataRequest) (string, error) {
	if err := authData.Validate(); err != nil {
		return "", err
	}

	authData.EncodeState()
	var buf bytes.Buffer
	err := json.NewEncoder(&buf).Encode(authData)
	if err != nil {
		return "", err
	}

	response, err := makeRequest("POST", apiUrl+requestTokenAction, &buf)
	if err != nil {
		return "", err
	}

	if response != nil {
		defer response.Close()
	}

	type TokenStruct struct {
		Code string `json:"code"`
	}

	tokenData := &TokenStruct{}
	err = json.NewDecoder(response).Decode(&tokenData)
	if err != nil {
		return "", err
	}

	return tokenData.Code, err
}

func RequestAccessToken(consumerKey, tokenCode string) (*AuthorizeDataResponse, error) {
	convertAccessToken := &ConvertTokenToAccessToken{
		ConsumerKey: consumerKey,
		Code:        tokenCode,
	}

	var buf bytes.Buffer
	err := json.NewEncoder(&buf).Encode(convertAccessToken)
	if err != nil {
		return nil, err
	}

	response, err := makeRequest("POST", apiUrl+accessTokenAction, &buf)
	if err != nil {
		return nil, err
	}

	if response != nil {
		defer response.Close()
	}

	accessToken := &AuthorizeDataResponse{}
	err = json.NewDecoder(response).Decode(&accessToken)
	if err != nil {
		return nil, err
	}

	return accessToken, err
}

func Add(addR *AddLinkRequest) error {
	if err := addR.Validate(); err != nil {
		return err
	}

	addR.Prepare()
	var buf bytes.Buffer
	err := json.NewEncoder(&buf).Encode(addR)
	if err != nil {
		return err
	}

	response, err := makeRequest("POST", apiUrl+addAction, &buf)
	if response != nil {
		defer response.Close()
	}

	if err != nil {
		return err
	}

	return nil
}

func Retrieve(listR *ItemListRequest) ([]*Item, error) {
	if err := listR.Validate(); err != nil {
		return nil, err
	}

	var buf bytes.Buffer
	err := json.NewEncoder(&buf).Encode(listR)
	if err != nil {
		return nil, err
	}

	response, err := makeRequest("POST", apiUrl+retrieveAction, &buf)
	if response != nil {
		defer response.Close()
	}

	if err != nil {
		return nil, err
	}

	list := &ItemListResponse{}
	err = json.NewDecoder(response).Decode(&list)
	if err != nil {
		return nil, err
	}

	return list.prepare(), nil
}

func PocketRedirectUrl(authData *AuthorizeDataRequest, requestToken string) string {
	return fmt.Sprintf("https://getpocket.com/auth/authorize?request_token=%s&redirect_uri=%s?state=%s",
		requestToken, authData.RedirectUrl, authData.ConvertedState,
	)
}

func ParseState(state string) map[string]string {
	parsedState := map[string]string{}
	explodedState := strings.Split(state, ";")
	for _, val := range explodedState {
		explodedVal := strings.Split(val, ":")
		if len(explodedVal) == 1 {
			parsedState[explodedVal[0]] = ""
		} else {
			parsedState[explodedVal[0]] = explodedVal[1]
		}
	}

	return parsedState
}

func makeRequest(method string, url string, body io.Reader) (io.ReadCloser, error) {
	client := &http.Client{}
	request, err := http.NewRequest(method, url, body)
	if err != nil {
		return nil, err
	}

	request.Header.Set("Content-Type", "application/json")
	request.Header.Set("charset", "UTF8")
	request.Header.Set("X-Accept", "application/json")
	response, _ := client.Do(request)
	if response.StatusCode != http.StatusOK {
		defer response.Body.Close()
		var errData string
		if response.Header.Get("X-Error-Code") != "" {
			errData = response.Header.Get("X-Error-Code")
		} else {
			bodyData, _ := io.ReadAll(response.Body)
			errData = string(bodyData)
		}

		errMsg := fmt.Sprintf("Request was not success. Error code: %d, error message: %s", response.StatusCode, errData)
		return nil, errors.New(errMsg)
	}

	return response.Body, nil
}
