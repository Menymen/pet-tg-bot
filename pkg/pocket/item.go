package pocket

import (
	"errors"
	"strconv"
)

const (
	ItemStatusActive     = "active"
	ItemStatusArchived   = "archived"
	ItemStatusPreDeleted = "pre_deleted"
)

type (
	ItemListRequest struct {
		ConsumerKey string `json:"consumer_key"`
		AccessToken string `json:"access_token"`
		State       string `json:"state,omitempty"`
		Favorite    bool   `json:"favorite,omitempty"`
		Tag         string `json:"tag,omitempty"`
		ContentType string `json:"contentType,omitempty"`
		Sort        string `json:"sort,omitempty"`
		DetailType  string `json:"detailType,omitempty"`
		Search      string `json:"search,omitempty"`
		Domain      string `json:"domain,omitempty"`
		Since       string `json:"since,omitempty"`
		Count       int    `json:"count,omitempty"`
		Offset      int    `json:"offset,omitempty"`
	}

	ItemListResponse struct {
		Status int              `json:"status"`
		List   map[int]*RawItem `json:"list"`
	}

	RawItem struct {
		ItemId        string `json:"item_id"`
		ResolvedId    string `json:"resolved-id"`
		GivenUrl      string `json:"given_url"`
		ResolvedUrl   string `json:"resolved_url"`
		GivenTitle    string `json:"given_title"`
		ResolvedTitle string `json:"resolved_title"`
		Favorite      string `json:"favorite"`
		Status        string `json:"status"`
		Excerpt       string `json:"excerpt"`
		IsArticle     string `json:"is_article"`
		HasImage      string `json:"has_image"`
		HasVideo      string `json:"has_video"`
		WordCount     string `json:"word_count"`
	}

	Item struct {
		ItemId        int    `json:"item_id"`
		ResolvedId    int    `json:"resolved-id"`
		GivenUrl      string `json:"given_url"`
		ResolvedUrl   string `json:"resolved_url"`
		GivenTitle    string `json:"given_title"`
		ResolvedTitle string `json:"resolved_title"`
		Favorite      bool   `json:"favorite"`
		Status        string `json:"status"`
		Excerpt       string `json:"excerpt"`
		IsArticle     bool   `json:"is_article"`
		HasImage      bool   `json:"has_image"`
		HasVideo      bool   `json:"has_video"`
		WordCount     int    `json:"word_count"`
	}
)

func (listR *ItemListRequest) Validate() error {
	if listR.ConsumerKey == "" {
		return errors.New("consumer key is required")
	}

	if listR.AccessToken == "" {
		return errors.New("access token is required")
	}

	return nil
}

func (rawList *ItemListResponse) prepare() []*Item {
	preparedItems := make([]*Item, 0, len(rawList.List))
	for _, rawItem := range rawList.List {
		preparedItems = append(preparedItems, rawItem.prepare())
	}

	return preparedItems
}

func (rawItem *RawItem) prepare() *Item {
	item := &Item{}
	item.ItemId, _ = strconv.Atoi(rawItem.ItemId)
	item.ResolvedId, _ = strconv.Atoi(rawItem.ResolvedId)
	item.GivenUrl = rawItem.GivenUrl
	item.ResolvedUrl = rawItem.ResolvedUrl
	item.GivenTitle = rawItem.GivenTitle
	item.ResolvedTitle = rawItem.ResolvedTitle
	if rawItem.Favorite == "1" {
		item.Favorite = true
	} else {
		item.Favorite = false
	}

	switch rawItem.Status {
	case "0":
		item.Status = ItemStatusActive
	case "1":
		item.Status = ItemStatusArchived
	case "2":
		item.Status = ItemStatusPreDeleted

	}

	item.Excerpt = rawItem.Excerpt
	if rawItem.IsArticle == "1" {
		item.IsArticle = true
	} else {
		item.IsArticle = false
	}

	if rawItem.HasImage == "1" {
		item.HasImage = true
	} else {
		item.HasImage = false
	}

	if rawItem.HasVideo == "1" {
		item.HasVideo = true
	} else {
		item.HasVideo = false
	}

	item.WordCount, _ = strconv.Atoi(rawItem.WordCount)

	return item
}
