package telegram_webhook

import (
	"context"
	"fmt"
	"github.com/sirupsen/logrus"
	"net/http"
	"time"
)

type Config struct {
	Port int
}

type Server struct {
	Cfg        *Config
	Handler    http.Handler
	httpServer *http.Server
}

func New(cfg *Config, handler http.Handler) *Server {
	return &Server{
		Cfg:     cfg,
		Handler: handler,
	}
}

func (server *Server) Run() error {
	port := fmt.Sprintf(":%d", server.Cfg.Port)
	server.httpServer = &http.Server{
		Addr:    port,
		Handler: server.Handler,
	}

	err := server.httpServer.ListenAndServe()
	if err != nil {
		return err
	}

	return nil
}

func (server *Server) Shutdown(ctx context.Context) {
	ctx, cancel := context.WithTimeout(ctx, 5*time.Second)
	defer cancel()

	if err := server.httpServer.Shutdown(ctx); err != nil {
		panic(err)
	} else {
		logrus.Println("application shutdowned")
	}
}
