package pocket

import (
	"context"
	"fmt"
	"github.com/gorilla/mux"
	"github.com/sirupsen/logrus"
	"net/http"
	"time"
)

type Config struct {
	Port int
}

type Server struct {
	Cfg        *Config
	Router     *mux.Router
	httpServer *http.Server
}

func New(cfg *Config, router *mux.Router) *Server {
	return &Server{
		Cfg:    cfg,
		Router: router,
	}
}

func (server *Server) Run() error {
	port := fmt.Sprintf(":%d", server.Cfg.Port)
	server.httpServer = &http.Server{
		Addr:    port,
		Handler: server.Router,
	}

	err := server.httpServer.ListenAndServe()
	if err != nil {
		return err
	}

	return nil
}

func (server *Server) Shutdown(ctx context.Context) {
	ctx, cancel := context.WithTimeout(ctx, 5*time.Second)
	defer cancel()

	if err := server.httpServer.Shutdown(ctx); err != nil {
		panic(err)
	} else {
		logrus.Println("application shutdowned")
	}
}
