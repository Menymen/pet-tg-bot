package test

import (
	"PetTgBot/internal/pocket"
	"github.com/stretchr/testify/assert"
	"testing"
)

var (
	validConsumerKey   = "105344-6b3ece1b56e64610dfeed06"
	invalidConsumerKey = "123"
	validAccessToken   = "4ed78a74-ffe7-4be0-7524-6e9a3d"
	invalidAccessToken = "123"
	requestUrl         = "https://google.com"
)

func TestAuthorizeDataValidate(t *testing.T) {
	type testCase struct {
		Data          *pocket.AuthorizeDataRequest
		ExpectedError bool
		ErrorMsg      string
	}

	testingData := []*testCase{
		{
			Data:          pocket.NewAuthData("", "", nil),
			ExpectedError: true,
			ErrorMsg:      "Must be error validate, all args are empty",
		},
		{
			Data:          pocket.NewAuthData(invalidConsumerKey, "", nil),
			ExpectedError: true,
			ErrorMsg:      "Must be error validate, redirect url is empty",
		},
		{
			Data:          pocket.NewAuthData("", "123", nil),
			ExpectedError: true,
			ErrorMsg:      "Must be error validate, consumer key is empty",
		},
		{
			Data:          pocket.NewAuthData(validConsumerKey, requestUrl, nil),
			ExpectedError: false,
			ErrorMsg:      "Must be success",
		},
	}

	for _, datum := range testingData {
		err := datum.Data.Validate()
		assert.Equal(t, datum.ExpectedError, err != nil, datum.ErrorMsg)
	}
}

func TestAuthorizeDataEncodeState(t *testing.T) {
	type testCase struct {
		Data          *pocket.AuthorizeDataRequest
		expectedValue string
	}

	testingData := []*testCase{
		{
			Data: pocket.NewAuthData("", "", map[string]string{
				"third":  "3",
				"one":    "1",
				"second": "2",
			}),
			expectedValue: "one:1;second:2;third:3",
		},
		{
			Data: pocket.NewAuthData("", "", map[string]string{
				"one": "1",
			}),
			expectedValue: "one:1",
		},
		{
			Data: pocket.NewAuthData("", "", map[string]string{
				"third":  "3",
				"":       "1",
				"second": "2",
			}),
			expectedValue: "second:2;third:3",
		},
		{
			Data: pocket.NewAuthData("", "", map[string]string{
				"third":  "3",
				"one":    "",
				"second": "2",
			}),
			expectedValue: "second:2;third:3",
		},
		{
			Data: pocket.NewAuthData("", "", map[string]string{
				"third":  "3",
				"":       "",
				"second": "2",
			}),
			expectedValue: "second:2;third:3",
		},
	}

	for _, datum := range testingData {
		datum.Data.EncodeState()
		assert.Equal(t, datum.expectedValue, datum.Data.ConvertedState)
	}
}

func TestRequestTokenCode(t *testing.T) {
	type testCase struct {
		Data          *pocket.AuthorizeDataRequest
		ExpectedError bool
		ErrorMsg      string
	}

	testingData := []*testCase{
		{
			Data:          pocket.NewAuthData("", "", nil),
			ExpectedError: true,
			ErrorMsg:      "Must be error validate, all args are empty",
		},
		{
			Data:          pocket.NewAuthData(invalidConsumerKey, "", nil),
			ExpectedError: true,
			ErrorMsg:      "Must be error validate, redirect url is empty",
		},
		{
			Data:          pocket.NewAuthData("", "123", nil),
			ExpectedError: true,
			ErrorMsg:      "Must be error validate, consumer key is empty",
		},
		{
			Data:          pocket.NewAuthData(invalidConsumerKey, requestUrl, nil),
			ExpectedError: true,
			ErrorMsg:      "Must be error, invalid consumer key",
		},
		{
			Data:          pocket.NewAuthData(validConsumerKey, requestUrl, nil),
			ExpectedError: false,
			ErrorMsg:      "Must be success",
		},
	}

	for _, datum := range testingData {
		_, err := pocket.RequestTokenCode(datum.Data)
		assert.Equal(t, datum.ExpectedError, err != nil, datum.ErrorMsg)
	}
}

func TestRequestAccessToken(t *testing.T) {
	type testCase struct {
		ConsumerKey   string
		TokenCode     string
		ExpectedError bool
		ErrorMsg      string
	}

	tokenCode := "123"
	testingData := []*testCase{
		{
			ExpectedError: true,
			ErrorMsg:      "Must be error validate, args is empty",
		},
		{
			ConsumerKey:   invalidConsumerKey,
			TokenCode:     tokenCode,
			ExpectedError: true,
			ErrorMsg:      "Must be error validate, invalid consumer key",
		},
		{
			ConsumerKey:   validConsumerKey,
			TokenCode:     tokenCode,
			ExpectedError: true,
			ErrorMsg:      "Must be error validate, invalid token code",
		},
	}

	for _, datum := range testingData {
		_, err := pocket.RequestAccessToken(datum.ConsumerKey, datum.TokenCode)
		assert.Equal(t, datum.ExpectedError, err != nil, datum.ErrorMsg)
	}
}

func TestAddRequestValidate(t *testing.T) {
	type testCase struct {
		Data          *pocket.AddLinkRequest
		ExpectedError bool
		ErrorMsg      string
	}

	testingData := []*testCase{
		{
			Data:          &pocket.AddLinkRequest{},
			ExpectedError: true,
			ErrorMsg:      "Must be error validate, all args are empty",
		},
		{
			Data: &pocket.AddLinkRequest{
				AccessToken: validAccessToken,
				ConsumerKey: validConsumerKey,
			},
			ExpectedError: true,
			ErrorMsg:      "Must be error validate, url is empty",
		},
		{
			Data: &pocket.AddLinkRequest{
				AccessToken: validAccessToken,
				Url:         requestUrl,
			},
			ExpectedError: true,
			ErrorMsg:      "Must be error validate, consumer key is empty",
		},
		{
			Data: &pocket.AddLinkRequest{
				ConsumerKey: validConsumerKey,
				Url:         requestUrl,
			},
			ExpectedError: true,
			ErrorMsg:      "Must be error validate, access token is empty",
		},
		{
			Data: &pocket.AddLinkRequest{
				AccessToken: validAccessToken,
				ConsumerKey: validConsumerKey,
				Url:         requestUrl,
			},
			ExpectedError: false,
			ErrorMsg:      "Must be success",
		},
	}

	for _, datum := range testingData {
		err := datum.Data.Validate()
		assert.Equal(t, datum.ExpectedError, err != nil, datum.ErrorMsg)
	}
}

func TestAddRequestPrepare(t *testing.T) {
	type testCase struct {
		Data          *pocket.AddLinkRequest
		expectedValue string
	}

	testingData := []*testCase{
		{
			Data:          &pocket.AddLinkRequest{},
			expectedValue: "",
		},
		{
			Data: &pocket.AddLinkRequest{
				TagsSlice: []string{"one", "two", "three"},
			},
			expectedValue: "one,two,three",
		},
		{
			Data: &pocket.AddLinkRequest{
				TagsSlice: []string{"one", "two"},
			},
			expectedValue: "one,two",
		},
		{
			Data: &pocket.AddLinkRequest{
				TagsSlice: []string{"one"},
			},
			expectedValue: "one",
		},
	}

	for _, datum := range testingData {
		datum.Data.Prepare()
		assert.Equal(t, datum.expectedValue, datum.Data.Tags)
	}
}

func TestAdd(t *testing.T) {
	type testCase struct {
		Data          *pocket.AddLinkRequest
		ExpectedError bool
		ErrorMsg      string
	}

	testingData := []*testCase{
		{
			Data:          &pocket.AddLinkRequest{},
			ExpectedError: true,
			ErrorMsg:      "Must be error validate, all args are empty",
		},
		{
			Data: &pocket.AddLinkRequest{
				Url:         requestUrl,
				ConsumerKey: validConsumerKey,
			},
			ExpectedError: true,
			ErrorMsg:      "Must be error validate, access token is empty",
		},
		{
			Data: &pocket.AddLinkRequest{
				Url:         requestUrl,
				AccessToken: validAccessToken,
			},
			ExpectedError: true,
			ErrorMsg:      "Must be error validate, consumer key is empty",
		},
		{
			Data: &pocket.AddLinkRequest{
				ConsumerKey: validConsumerKey,
				AccessToken: validAccessToken,
			},
			ExpectedError: true,
			ErrorMsg:      "Must be error validate, url is empty",
		},
		{
			Data: &pocket.AddLinkRequest{
				Url:         requestUrl,
				ConsumerKey: invalidConsumerKey,
				AccessToken: validAccessToken,
			},
			ExpectedError: true,
			ErrorMsg:      "Must be error, invalid consumer key",
		},
		{
			Data: &pocket.AddLinkRequest{
				Url:         requestUrl,
				ConsumerKey: validConsumerKey,
				AccessToken: invalidAccessToken,
			},
			ExpectedError: true,
			ErrorMsg:      "Must be error, invalid access token",
		},
		{
			Data: &pocket.AddLinkRequest{
				Url:         "123",
				ConsumerKey: validConsumerKey,
				AccessToken: validAccessToken,
			},
			ExpectedError: true,
			ErrorMsg:      "Must be error, invalid url",
		},
		{
			Data: &pocket.AddLinkRequest{
				Url:         requestUrl,
				ConsumerKey: validConsumerKey,
				AccessToken: validAccessToken,
			},
			ExpectedError: false,
			ErrorMsg:      "Must be success",
		},
	}

	for _, datum := range testingData {
		err := pocket.Add(datum.Data)
		assert.Equal(t, datum.ExpectedError, err != nil, datum.ErrorMsg)
	}
}

func TestRetrieveValidate(t *testing.T) {
	type testCase struct {
		Data          *pocket.ItemListRequest
		ExpectedError bool
		ErrorMsg      string
	}

	testingData := []*testCase{
		{
			Data:          &pocket.ItemListRequest{},
			ExpectedError: true,
			ErrorMsg:      "Must be error validate, all args are empty",
		},
		{
			Data: &pocket.ItemListRequest{
				AccessToken: validAccessToken,
			},
			ExpectedError: true,
			ErrorMsg:      "Must be error validate, consumer key is empty",
		},
		{
			Data: &pocket.ItemListRequest{
				ConsumerKey: validConsumerKey,
			},
			ExpectedError: true,
			ErrorMsg:      "Must be error validate, access token is empty",
		},
		{
			Data: &pocket.ItemListRequest{
				AccessToken: validAccessToken,
				ConsumerKey: validConsumerKey,
			},
			ExpectedError: false,
			ErrorMsg:      "Must be success",
		},
	}

	for _, datum := range testingData {
		err := datum.Data.Validate()
		assert.Equal(t, datum.ExpectedError, err != nil, datum.ErrorMsg)
	}
}

func TestRetrieve(t *testing.T) {
	type testCase struct {
		Data          *pocket.ItemListRequest
		ExpectedError bool
		ErrorMsg      string
	}

	testingData := []*testCase{
		{
			Data:          &pocket.ItemListRequest{},
			ExpectedError: true,
			ErrorMsg:      "Must be error validate, all args are empty",
		},
		{
			Data: &pocket.ItemListRequest{
				ConsumerKey: validConsumerKey,
			},
			ExpectedError: true,
			ErrorMsg:      "Must be error validate, access token is empty",
		},
		{
			Data: &pocket.ItemListRequest{
				AccessToken: validAccessToken,
			},
			ExpectedError: true,
			ErrorMsg:      "Must be error validate, consumer key is empty",
		},
		{
			Data: &pocket.ItemListRequest{
				ConsumerKey: invalidConsumerKey,
				AccessToken: validAccessToken,
			},
			ExpectedError: true,
			ErrorMsg:      "Must be error, invalid consumer key",
		},
		{
			Data: &pocket.ItemListRequest{
				ConsumerKey: validConsumerKey,
				AccessToken: invalidAccessToken,
			},
			ExpectedError: true,
			ErrorMsg:      "Must be error, invalid access token",
		},
		{
			Data: &pocket.ItemListRequest{
				ConsumerKey: validConsumerKey,
				AccessToken: validAccessToken,
			},
			ExpectedError: false,
			ErrorMsg:      "Must be success",
		},
	}

	for _, datum := range testingData {
		_, err := pocket.Retrieve(datum.Data)
		assert.Equal(t, datum.ExpectedError, err != nil, datum.ErrorMsg)
	}
}
