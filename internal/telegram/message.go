package telegram

type MessageUpdate struct {
	UpdateId int      `json:"update_id"`
	Message  *Message `json:"message"`
}

type Message struct {
	MessageId int64        `json:"message_id"`
	From      *MessageFrom `json:"from"`
	Chat      *MessageChat `json:"chat"`
	Date      int64        `json:"date"`
	Text      string       `json:"text"`
}

type MessageFrom struct {
	Id           int64  `json:"id"`
	IsBot        bool   `json:"is_bot"`
	FirstName    string `json:"first_name"`
	LastName     string `json:"last_name"`
	Username     string `json:"username"`
	LanguageCode string `json:"language_code"`
	IsPremium    bool   `json:"is_premium"`
}

type MessageChat struct {
	Id        int64  `json:"id"`
	FirstName string `json:"first_name"`
	LastName  string `json:"last_name"`
	Username  string `json:"username"`
	Type      string `json:"type"`
}
