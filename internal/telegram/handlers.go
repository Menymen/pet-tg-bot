package telegram

import (
	"PetTgBot/internal/models"
	"PetTgBot/internal/services"
	"PetTgBot/pkg/pocket"
	"fmt"
	"github.com/sirupsen/logrus"
	"strconv"
	"strings"
	"sync"
	"time"
)

const (
	commandStart      = "start"
	commandEnd        = "end"
	commandAuthPocket = "authPocket"
	commandAddLink    = "addPocketLink"
	commandLinkList   = "pocketLinkList"
)

type PocketConfig struct {
	ConsumerKey string
	CallbackUrl string
}

type Handler struct {
	Service   services.TelegramServiceInterface
	Sender    SenderInterface
	PocketCnf *PocketConfig
}

type HandlerInterface interface {
	ProcessCommand(message *Message) error
}

func NewPocketCnf(consumerKey, callbackUrl string) *PocketConfig {
	return &PocketConfig{
		ConsumerKey: consumerKey,
		CallbackUrl: callbackUrl,
	}
}

func NewHandler(service services.TelegramServiceInterface, sender SenderInterface, pocketCnf *PocketConfig) HandlerInterface {
	return &Handler{
		Service:   service,
		Sender:    sender,
		PocketCnf: pocketCnf,
	}
}

func (handler *Handler) ProcessCommand(message *Message) error {
	wg := sync.WaitGroup{}
	wg.Add(2)
	go func() {
		err := handler.Service.SaveMessage(convertTelegramMessageToModels(message))
		if err != nil {
			logrus.Errorf("SQL error: %v", err)
		}

		wg.Done()
	}()

	go func() {
		var err error
		command := strings.TrimLeft(message.Text, "/")
		subWg := sync.WaitGroup{}
		subWg.Add(1)
		go func() {
			var sqlErr error
			if command == commandEnd {
				sqlErr = handler.Service.DisableChat(message.Chat.Id)
			} else {
				sqlErr = handler.Service.EnableChat(message.Chat.Id)
			}

			if sqlErr != nil {
				logrus.Errorf("SQL error: %v", sqlErr)
			}

			subWg.Done()
		}()

		switch command {
		case commandStart:
			err = handler.handleStart(message)
		case commandEnd:
			err = handler.handleEnd(message)
		case commandAuthPocket:
			err = handler.handleAuthPocket(message)
		case commandLinkList:
			err = handler.handleLinkList(message)
		default:
			err = handler.handleDefault(message)
		}

		if err != nil {
			logrus.Errorf("Request error: %v", err)
		}

		subWg.Wait()
		wg.Done()
	}()

	wg.Wait()
	return nil
}

func (handler *Handler) handleStart(message *Message) error {
	sendMessage := handler.generateMessage(message.Chat.Id, "Thank for activate our bot. Use keyboard or /help command", nil, false)
	return handler.Sender.SendMessage(sendMessage)
}

func (handler *Handler) handleEnd(message *Message) error {
	sendMessage := handler.generateMessage(
		message.Chat.Id,
		"Bot deactivated. I will dont send you any messages",
		[]string{"/start"},
		true,
	)
	return handler.Sender.SendMessage(sendMessage)
}

func (handler *Handler) handleLinkList(message *Message) error {
	accessToken, accessTokenIsValid := handler.findFindValidAccessToken(message.Chat.Id)
	var list []*pocket.Item
	var err error
	if accessTokenIsValid {
		listRequest := &pocket.ItemListRequest{
			ConsumerKey: handler.PocketCnf.ConsumerKey,
			AccessToken: accessToken,
		}

		list, err = pocket.Retrieve(listRequest)
		if err != nil {
			logrus.Errorf("Error whiling request link list: %v", err)
			accessTokenIsValid = false
		}
	}

	var sendMessage *SendMessage
	if accessTokenIsValid {
		fmt.Println(list)
	} else {
		text := fmt.Sprintf("You access token is invalid. Please use command /%s for auth your account", commandAuthPocket)
		sendMessage = handler.generateMessage(message.Chat.Id, text, nil, false)
		return handler.Sender.SendMessage(sendMessage)
	}

	return handler.Sender.SendMessage(sendMessage)
}

func (handler *Handler) handleAuthPocket(message *Message) error {
	state := map[string]string{"id": strconv.Itoa(int(message.Chat.Id))}
	pocketAuthData := pocket.NewAuthData(handler.PocketCnf.ConsumerKey, handler.PocketCnf.CallbackUrl, state)
	pocketRequestToken, err := pocket.RequestTokenCode(pocketAuthData)
	if err != nil {
		logrus.Errorf("Error whiling generate pocket request token: %v", err)
		sendMessage := handler.generateMessage(
			message.Chat.Id,
			"Service temporary unavailable",
			nil,
			false,
		)

		return handler.Sender.SendMessage(sendMessage)
	}

	pocketAuthLink := pocket.PocketRedirectUrl(pocketAuthData, pocketRequestToken)

	text := fmt.Sprintf(
		"Use link for auth you pocket account: %s",
		pocketAuthLink,
	)

	sendMessage := handler.generateMessage(
		message.Chat.Id,
		text,
		nil,
		false,
	)

	return handler.Sender.SendMessage(sendMessage)
}

func (handler *Handler) handleDefault(message *Message) error {
	sendMessage := handler.generateMessage(message.Chat.Id, "Undefined command, please use your keyboard", nil, false)
	return handler.Sender.SendMessage(sendMessage)
}

func (handler *Handler) generateMessage(chatId int64, text string, additionalCommands []string, eraseBase bool) *SendMessage {
	sendMessage := &SendMessage{
		ChatId:      chatId,
		Text:        text,
		ReplyMarkup: handler.markupKeyboard(chatId, additionalCommands, eraseBase),
	}

	return sendMessage
}

func (handler *Handler) markupKeyboard(chatId int64, additionalCommands []string, eraseBase bool) *ReplyKeyboardMarkup {
	var commands []string
	if !eraseBase {
		commands = handler.commandList(chatId)
	}

	if len(additionalCommands) > 0 {
		for _, val := range additionalCommands {
			commands = append(commands, val)
		}
	}

	return handler.Sender.GenerateReplyMarkup(commands)
}

func (handler *Handler) commandList(chatId int64) []string {
	var commands []string
	_, isValidToken := handler.findFindValidAccessToken(chatId)
	if isValidToken {
		commands = append(commands, []string{commandLinkList, commandAddLink}...)
	} else {
		commands = append(commands, commandAuthPocket)
	}

	return addSlashes(commands)
}

func addSlashes(commands []string) []string {
	addSlashCommands := make([]string, len(commands), len(commands))
	for i, val := range commands {
		addSlashCommands[i] = "/" + val
	}

	return addSlashCommands
}

func convertTelegramMessageToModels(message *Message) (*models.TelegramMessage, *models.TelegramFrom, *models.TelegramChat) {
	date := time.Unix(message.Date, 0)
	telegramMessage := &models.TelegramMessage{
		Id:     message.MessageId,
		FromId: message.From.Id,
		ChatId: message.Chat.Id,
		Date:   date.Format("2006-01-02 15:04:05"),
		Text:   message.Text,
	}

	telegramFrom := &models.TelegramFrom{
		Id:           message.From.Id,
		IsBot:        message.From.IsBot,
		FirstName:    message.From.FirstName,
		LastName:     message.From.LastName,
		Username:     message.From.Username,
		LanguageCode: message.From.LanguageCode,
		IsPremium:    message.From.IsPremium,
	}

	telegramChat := &models.TelegramChat{
		Id:        message.Chat.Id,
		FirstName: message.Chat.FirstName,
		LastName:  message.Chat.LastName,
		Username:  message.Chat.Username,
		Type:      message.Chat.Type,
	}

	return telegramMessage, telegramFrom, telegramChat
}

func (handler *Handler) findFindValidAccessToken(chatId int64) (accessToken string, isValid bool) {
	var err error
	accessToken, err = handler.Service.GetChatPocketAccessKey(chatId)
	if err != nil {
		logrus.Errorf("Error whiling find access token: %v", err)
		isValid = false
	} else {
		isValid = true
	}

	if accessToken == "" {
		isValid = false
	}

	return accessToken, isValid
}
