package telegram

import (
	"bytes"
	"encoding/json"
	"errors"
	"fmt"
	"io"
	"net/http"
)

const baseUrl = "https://api.telegram.org/bot%s/%s"

type Sender struct {
	ApiKey string
}

type SenderInterface interface {
	SendMessage(message *SendMessage) error
	GenerateReplyMarkup(buttons []string) *ReplyKeyboardMarkup
}

type SendMessage struct {
	ChatId      int64  `json:"chat_id"`
	Text        string `json:"text"`
	ReplyMarkup any    `json:"reply_markup,omitempty"`
}

type ReplyKeyboardMarkup struct {
	Keyboard       [][]*ReplyKeyboardButton `json:"keyboard,omitempty"`
	IsPersistent   bool                     `json:"is_persistent,omitempty"`
	ResizeKeyboard bool                     `json:"resize_keyboard,omitempty"`
}

type ReplyKeyboardButton struct {
	Text string `json:"text"`
}

func NewSender(apiKey string) SenderInterface {
	return &Sender{ApiKey: apiKey}
}

func (s *Sender) SendMessage(message *SendMessage) error {
	var buf bytes.Buffer
	err := json.NewEncoder(&buf).Encode(message)
	if err != nil {
		return err
	}

	return makeRequest("sendMessage", "POST", s.ApiKey, &buf)
}

func (s *Sender) GenerateReplyMarkup(buttons []string) *ReplyKeyboardMarkup {
	subKeyboard := make([]*ReplyKeyboardButton, 0, len(buttons))
	for _, button := range buttons {
		subKeyboard = append(subKeyboard, generateKeyboardButton(button))
	}

	fullKeyboard := make([][]*ReplyKeyboardButton, 1, 1)
	fullKeyboard[0] = subKeyboard
	keyboard := &ReplyKeyboardMarkup{
		Keyboard:       fullKeyboard,
		IsPersistent:   true,
		ResizeKeyboard: true,
	}

	return keyboard
}

func generateKeyboardButton(text string) *ReplyKeyboardButton {
	return &ReplyKeyboardButton{Text: text}
}

func makeRequest(methodName string, method string, apiKey string, body io.Reader) error {
	client := &http.Client{}
	requestUrl := fmt.Sprintf(baseUrl, apiKey, methodName)
	request, err := http.NewRequest(method, requestUrl, body)
	if err != nil {
		return err
	}

	request.Header.Set("Content-Type", "application/json")
	request.Header.Set("Charset", "UTF8")
	response, _ := client.Do(request)
	defer response.Body.Close()
	if response.StatusCode != http.StatusOK {
		var errData string
		bodyData, _ := io.ReadAll(response.Body)
		errData = string(bodyData)

		errMsg := fmt.Sprintf("Request was not success. Error code: %d, error message: %s", response.StatusCode, errData)
		return errors.New(errMsg)
	}

	return nil
}
