package services

import (
	"PetTgBot/internal/models"
	"PetTgBot/internal/storage"
)

type TelegramServiceInterface interface {
	SaveMessage(message *models.TelegramMessage, from *models.TelegramFrom, chat *models.TelegramChat) error
	EnableChat(chatId int64) error
	DisableChat(chatId int64) error
	GetChatPocketAccessKey(chatId int64) (string, error)
}

type TelegramService struct {
	Storage storage.StorageInterface
}

func NewTelegramService(storage storage.StorageInterface) TelegramServiceInterface {
	return &TelegramService{
		Storage: storage,
	}
}

func (service *TelegramService) SaveMessage(message *models.TelegramMessage, from *models.TelegramFrom, chat *models.TelegramChat) error {
	return service.Storage.SaveMessage(message, from, chat)
}

func (service *TelegramService) EnableChat(chatId int64) error {
	return service.Storage.UpdateChatEnabled(chatId, true)
}

func (service *TelegramService) DisableChat(chatId int64) error {
	return service.Storage.UpdateChatEnabled(chatId, false)
}

func (service *TelegramService) GetChatPocketAccessKey(chatId int64) (string, error) {
	return service.Storage.GetChatPocketAccessKey(chatId)
}
