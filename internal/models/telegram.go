package models

type TelegramFrom struct {
	Id           int64
	IsBot        bool
	FirstName    string
	LastName     string
	Username     string
	LanguageCode string
	IsPremium    bool
	CreatedAt    string
	UpdatedAt    string
}

type TelegramChat struct {
	Id          int64
	FirstName   string
	LastName    string
	Username    string
	Type        string
	IsEnabled   bool
	CreatedAt   string
	UpdatedAt   string
	AccessToken string
	TokenCode   string
}

type TelegramMessage struct {
	Id        int64
	FromId    int64
	ChatId    int64
	Date      string
	Text      string
	CreatedAt string
	UpdatedAt string
	from      *TelegramFrom
	chat      *TelegramChat
}

func (message *TelegramMessage) SetFrom(from *TelegramFrom) {
	message.from = from
}

func (message *TelegramMessage) GetFrom() *TelegramFrom {
	return message.from
}

func (message *TelegramMessage) SetChat(chat *TelegramChat) {
	message.chat = chat
}

func (message *TelegramMessage) GetChat() *TelegramChat {
	return message.chat
}
