package postgres

import (
	"PetTgBot/internal/models"
	"PetTgBot/internal/storage"
	"database/sql"
	"fmt"
	"github.com/jmoiron/sqlx"
	_ "github.com/lib/pq"
	"strconv"
)

const (
	telegramMessageTableName = "telegram.message"
	telegramChatTableName    = "telegram.chat"
	telegramFromTableName    = "telegram.from"
)

type Config struct {
	Username      string
	Password      string
	Host          string
	Port          string
	DbName        string
	MaxConnection string
}

type DbConnection struct {
	db *sqlx.DB
}

func NewDb(cnf *Config) (storage.StorageInterface, error) {
	connStr := fmt.Sprintf(
		"host=%s port=%s dbname=%s user=%s password=%s sslmode=disable",
		cnf.Host, cnf.Port, cnf.DbName, cnf.Username, cnf.Password,
	)

	db, err := sqlx.Open("postgres", connStr)
	if err != nil {
		return nil, err
	}

	maxConnections, _ := strconv.Atoi(cnf.MaxConnection)
	db.SetMaxOpenConns(maxConnections)

	err = db.Ping()
	// Проверка соединения с базой данных
	if err != nil {
		return nil, err
	}

	dbApp := &DbConnection{db: db}

	return dbApp, nil
}

func (conn *DbConnection) Close() {
	_ = conn.db.Close()
}

func (conn *DbConnection) SaveMessage(message *models.TelegramMessage, from *models.TelegramFrom, chat *models.TelegramChat) error {
	tx, err := conn.db.Begin()
	if err != nil {
		return err
	}

	insertFromQueryString := "INSERT INTO %s (id, is_bot, first_name, last_name, username, language_code, is_premium) " +
		"VALUES ($1, $2, $3, $4, $5, $6, $7) " +
		"ON CONFLICT (id) DO UPDATE SET " +
		"is_bot = $2, " +
		"first_name = $3, " +
		"last_name = $4, " +
		"username = $5, " +
		"language_code = $6, " +
		"is_premium = $7, " +
		"updated_at = NOW();"

	insertFromQuery := fmt.Sprintf(insertFromQueryString, telegramFromTableName)

	_, err = tx.Exec(
		insertFromQuery,
		from.Id,
		from.IsBot,
		from.FirstName,
		from.LastName,
		from.Username,
		from.LanguageCode,
		from.IsPremium,
	)

	if err != nil {
		tx.Rollback()
		return err
	}

	insertChatQueryString := "INSERT INTO %s (id, first_name, last_name, type, is_enabled) " +
		"VALUES ($1, $2, $3, $4, $5) " +
		"ON CONFLICT (id) DO UPDATE SET " +
		"first_name = $2, " +
		"last_name = $3, " +
		"type = $4, " +
		"updated_at = NOW();"

	insertChatQuery := fmt.Sprintf(insertChatQueryString, telegramChatTableName)
	_, err = tx.Exec(
		insertChatQuery,
		chat.Id,
		chat.FirstName,
		chat.LastName,
		chat.Type,
		true,
	)

	if err != nil {
		tx.Rollback()
		return err
	}

	insertMessageQueryString := "INSERT INTO %s (id, from_id, chat_id, date, text) VALUES ($1, $2, $3, $4, $5)"
	insertMessageQuery := fmt.Sprintf(insertMessageQueryString, telegramMessageTableName)
	_, err = tx.Exec(
		insertMessageQuery,
		message.Id,
		message.FromId,
		message.ChatId,
		message.Date,
		message.Text,
	)

	if err != nil {
		tx.Rollback()
		return err
	}

	err = tx.Commit()
	if err != nil {
		return err
	}

	return nil
}

func (conn *DbConnection) UpdateChatEnabled(chatId int64, isEnabled bool) error {
	queryString := "UPDATE %s SET is_enabled = $1 WHERE id = $2;"
	query := fmt.Sprintf(queryString, telegramChatTableName)

	_, err := conn.db.Exec(
		query,
		isEnabled,
		chatId,
	)

	return err
}

func (conn *DbConnection) GetChatPocketAccessKey(chatId int64) (string, error) {
	queryString := "SELECT access_token from %s WHERE id = $1 LIMIT 1"
	query := fmt.Sprintf(queryString, telegramChatTableName)

	var result sql.NullString
	err := conn.db.QueryRow(
		query,
		chatId,
	).Scan(&result)

	if err != nil || result.Valid == false {
		return "", err
	}

	return result.String, nil
}
