package storage

import "PetTgBot/internal/models"

type StorageInterface interface {
	Close()
	SaveMessage(message *models.TelegramMessage, from *models.TelegramFrom, chat *models.TelegramChat) error
	UpdateChatEnabled(chatId int64, isEnabled bool) error
	GetChatPocketAccessKey(chatId int64) (string, error)
}
